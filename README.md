# exfat-utils

Utilities for exFAT file system. This project aims to provide a
full-featured exFAT file system implementation for Unix-like systems
  
[Homepage](https://github.com/relan/exfat)

### Install:
```
kcp -i exfat-utils
```

#### Changelog:
[https://raw.githubusercontent.com/relan/exfat/master/ChangeLog](https://raw.githubusercontent.com/relan/exfat/master/ChangeLog)
